# Мониторинг

## Требования

- Нужен сервер с установленным Docker и compose плагином

## Доступ

<https://grafana.dev.todo-list-app.deusops.kromanyon.ru:3000> логин admin, пароль выдаст суперважный чел из СБ  
Сертификат временно самоподписанный, как только будет доступна еще одна ВМ с реальным IP, можно будет перейти на LetEncrypt

## Дополнительные ресурсы

<https://gitlab.com/kromany0n/deusops_exercises/240322_monitoring>  
<https://gitlab.com/kromany0n/deusops_exercises/240322_dev_host>  
